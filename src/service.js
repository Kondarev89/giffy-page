// async code

import { apiKey, limit, apiSearch, inpFile, apiUp, upKey } from './common.js';

const searchGif = async (word) => {
  let response = await fetch(`${apiSearch}${apiKey}${word}${limit}`);
  let data = await response.json();
  return data;
}

const getInfo = async (url) => {
  let response = await fetch(url);
  let data = await response.json();
  return data;
}

const postCallback = async (e) => {
  e.preventDefault();

  // upload;
  const formData = new FormData();

  formData.append('file', inpFile.files[0]);
  const url = `${apiUp}${upKey}`;

  try {
    const response = await fetch(url, {
      method: 'POST',
      body: formData,
      // mode: 'no-cors'
    })
    const json = await response.json();
  } catch (err) {
    console.log(err.message);
  }
}

export {
  searchGif,
  getInfo,
  postCallback,
}
