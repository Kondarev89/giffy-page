/* eslint-disable no-invalid-this */
import { CONTAINER, api, apiKey,
  apiTrending, limit } from './common.js'
import { searchGif, getInfo } from './service.js'
import { renderDivItem, renderDivWithoutBtn } from './templates.js'
import { addFavorite, getFavorites,
  removeFavorite, clearFavorites } from './favorites-service.js'


export const searchFn = async () => {
  CONTAINER.empty();
  const word = $('#search-bar').val()?$('#search-bar').val():'trend';
  const GIF = await searchGif(`&q=${word}`);
  GIF.data.forEach((item) => {
    const rating = item.rating;
    const username = item.username;
    CONTAINER.append(
        renderDivItem(item.id, item.images.fixed_height_downsampled.url,
            rating, username));
  });
  $('#search-bar').val('');
};


export const gifInfoHandlerOver = async (event) => {
  // const id = `&ids=${$(event.target).parent().attr('id')}`;
  // const infoFn = await getInfo(`${api}${id}${apiKey}`)
  // const rating = infoFn.data[0].rating;
  // const uploadBy = infoFn.data[0].username;
  // $(event.target).next().html(`rating: <span>${rating}</span>`)
  // $(event.target).next().next().html(`upload by: <span>${uploadBy}</span>`)

  $(event.target).next().css('display', 'block');
  $(event.target).next().next().css('display', 'block');
}

export const trendFn = async () => {
  $('#clear-fav').css('display', 'none')

  CONTAINER.empty();
  const data = await getInfo(`${apiTrending}${apiKey}${limit}`)
  data.data.forEach((item) => {
    const rating = item.rating;
    const username = item.username;
    CONTAINER.append(
        renderDivItem(item.id, item.images.fixed_height_downsampled.url,
            rating, username));
  })
};

export const gifInfoHandlerLeave = (event) => {
  $(event.target).next().css('display', 'none');
  $(event.target).next().next().css('display', 'none');
}

// export const infiniteScrollHandler = () => {
//   if ($(window).scrollTop() >=
// $(document).height() - $(window).height() - 10) {
//     getInfo(`${apiRandom}${apiKey}`).then((data) => {
//       CONTAINER.append(
//           renderDivItem(
// data.data.id, data.data.fixed_height_downsampled_url))
//     })
//   }
// }

export const favBTNcallback = function() {
  const id = $(this).parent().attr('id');
  const link = $(this).prev().prev().prev().attr('src');
  const rating = $(this).prev().prev().text().split(' ')[1];
  const username = $(this).prev().text().split(' ')[1];

  $(this).toggleClass('red');

  if ($(this).hasClass('red')) {
    addFavorite(id, link, rating, username)
  } else {
    removeFavorite(id);
  }
}

export const favBTNcontCallback = function() {
  $('#clear-fav').css('display', 'inline-block')

  $('#container').empty();
  getFavorites().forEach((obj) => {
    $('#container').append(
        renderDivWithoutBtn(obj.id, obj.link, obj.rating, obj.username));
  })
}

export const favClearCallback = async function() {
  $('#clear-fav').css('display', 'none')
  clearFavorites();
  $('#container').empty();
}

