import { postCallback } from './service.js'
import { gifInfoOver, gifInfoLeave, trendingBTN, favoriteClear,
  uploadGifs, searchBTN, favoriteBTNheart, favoriteBTNcontainer,
  searchEnter } from './events.js';
import { gifInfoHandlerOver, trendFn,
  favBTNcallback, favBTNcontCallback, favClearCallback,
  gifInfoHandlerLeave, searchFn } from './views.js'
  // infiniteScrollHandler,
  // infiniteScroll,


(async () => {
  trendFn();
  trendingBTN(trendFn)
  gifInfoOver(gifInfoHandlerOver)
  gifInfoLeave(gifInfoHandlerLeave)
  // infiniteScroll(infiniteScrollHandler)
  uploadGifs(postCallback)
  searchBTN(searchFn);
  searchEnter(searchFn);
  favoriteBTNheart(favBTNcallback)
  favoriteBTNcontainer(favBTNcontCallback)
  favoriteClear(favClearCallback)


})()
