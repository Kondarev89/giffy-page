const savedFavorites = localStorage.getItem('favorites');

let favorites = savedFavorites ? JSON.parse(savedFavorites) : [];

const saveFavorites = () => {
  localStorage.setItem('favorites', JSON.stringify(favorites));
};

export const getFavorites = () => favorites.slice();

export const addFavorite = (id, link, rating, username) => {
  favorites.push({
    id,
    link,
    rating,
    username
  });

  saveFavorites();
};

export const removeFavorite = (id) => {
  favorites = favorites.filter((c) => c.id !== id);

  saveFavorites();
};

export const clearFavorites = () => {
  favorites = [];

  saveFavorites();
};
